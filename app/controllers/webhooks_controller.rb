class WebhooksController < ApplicationController
	require 'openssl'

	def incoming
		########################################
		## When a user sets up a service, activate
		## the webhooks via API.
		## ?? - Do this at the model level???
		msg = "A step forward."
		########################################
		unless params[:service].blank?
			case params[:service]
				when "sendgrid"
					#begin
						sendgrid_params = params[:_json]
						#sendgrid_params = JSON.parse(params[:_json])
						sendgrid_params.each do |sendgrid_event|
							sent_mail = SentMail.find_by(ext_id: sendgrid_event[:ext_id])
							case sendgrid_event[:event]
								when "processed"
									sent_mail.sent = true
								when "dropped"
									sent_mail.dropped = Time.now
								when "delivered"
									sent_mail.delivered = Time.now
									sent_mail.ip_address = sendgrid_event[:ip]
								when "deferred"
									# Not sure what to do with this.
									# Is it a common metric?
								when "bounce"
									sent_mail.bounced = Time.now
								when "open"
									sent_mail.delay.save_open(sendgrid_event[:ip])
									triggers_test("open", sent_mail.id)
								when "click"
									sent_mail.delay.save_click(sendgrid_event["url"], sendgrid_event["ip"])

									triggers_test("click", sent_mail.id)
								when "spamreport"
									sent_mail.spam = Time.now
								when "unsubscribe"
									sent_mail.delay.save_unsubscribe(sendgrid_event[:ip])

									triggers_test("unsubscribe", sent_mail.id)
								when "group_unsubscribe"
									# Consider tracking group events sometime in the future.
									sent_mail.unsubscribe = Time.now

									triggers_test("unsubscribe", sent_mail.id)
								when "group_resubscribe"
									# Not sure what to do with this.
									# Is it a common metric?
									sent_mail.unsubscribe = nil
							end
			
							sent_mail.save
						end
					#rescue Exception => e
					#	logger.debug("Exception: #{e.message}")
					#end

				when "mandrill"
					# puts "Request URL: #{request.url}\nParams: #{params}\nKey: #{ENV['MANDRILL_WEBHOOK_KEY']}"
					# puts verify_mandrill_request_signature(request.url, params)
					puts "Mandrill Request Verification: #{verify_mandrill_request_signature(request.url, params)}"
					
					# Allow us to access the params using strings instead of symbols.
					#mandrill_params = params.with_indifferent_access
					#logger.debug("Event: #{mandrill_params["mandrill_events"]["event"]}")
					unless params[:mandrill_events].blank? || params[:mandrill_events] == "[]"
						puts "Mandrill Events: #{params[:mandrill_events]}"
						mandrill_params = JSON.parse(params[:mandrill_events])
						msg = "#{mandrill_params[0]["event"].to_s}"

						unless mandrill_params[0].blank?
							unless mandrill_params[0]["msg"]["_id"].blank?
								begin
									sent_mail = SentMail.find_by(ext_id: "#{mandrill_params[0]["msg"]["_id"]}")
			
									#send, deferral, hard_bounce, soft_bounce, open, click, spam, unsub, reject
									logger.info("Mandrill Event: #{mandrill_params[0]["event"].to_s}")
									case mandrill_params[0]["event"].to_s
										when "send"
											sent_mail.sent = true
										when "hard_bounce"
											sent_mail.bounced = Time.now
										when "soft_bounce"
											sent_mail.bounced = Time.now
										when "open"
											sent_mail.delivered = Time.now
											sent_mail.delay.save_open(mandrill_params[0]['ip'])

											triggers_test("open", sent_mail.id)
										when "spam"
											sent_mail.spam = Time.now
										when "unsub"
											sent_mail.delay.save_unsubscribe(mandrill_params[0]['ip'])

											triggers_test("unsubscribe", sent_mail.id)
										when "reject"
											sent_mail.blocked = Time.now
										when "click"
											sent_mail.delay.save_click(mandrill_params[0]["mandrill_events"]["clicks_detail"], mandrill_params[0]['ip'])	
									end

									sent_mail.save
								rescue Exception => e
									logger.debug("Exception: #{e.message}")
								end
							end
						end
					end
	
				when "mailgun"
					sent_mail = SentMail.find_by(ext_id: params["Message-Id"])
					if sent_mail
						case params[:event]
							when "opened"
								sent_mail.delay.save_open(params[:ip])
							when "clicked"
								sent_mail.delay.save_click(params[:url], params[:ip])
							when "unsubscribed"
								sent_mail.delay.save_unsubscribe(params[:ip])
							when "complained"
								sent_mail.spam = Time.now
							when "bounced"
								sent_mail.bounced = Time.now
							when "delivered"
								sent_mail.delivered = Time.now
							when "dropped"
								sent_mail.dropped = Time.now
						end
						sent_mail.save
					end						
	
				when "mailjet"
					sent_mail = SentMail.find_by(ext_id: params[:MessageID])
					
					case params[:event]
						when "open"
							sent_mail.delay.save_open(params[:ip])
							sent_mail.delivered = Time.now

							triggers_test("open", sent_mail.id)

						when "click"
							sent_mail.delay.save_click(params[:url], params[:ip])

						when "bounce"
							sent_mail.bounced = Time.now

						when "spam"
							sent_mail.spam = Time.now

						when "blocked"
							sent_mail.blocked = Time.now

						when "unsub"
							sent_mail.delay.save_unsubscribe(params[:ip])
					end

					sent_mail.save

				when "ses"
					# Amazon SES
					# Using internal "lite" tracking - no webhooks to set up.
	
				when "postage"
					# Postage
					# Using internal "lite" tracking - no webhooks to set up.
	
				when "socketlabs"
					sent_mail = SentMail.find_by(ext_id: params[:MessageId])
					if sent_mail
						case params[:Type]
							when "Delivered"
								sent_mail.delivered = Time.now
							when "Tracking"
								sent_mail.delivered = Time.now
								case params[:TrackingType]
									when 0
										sent_mail.delay.save_click(params[:Url], params[:ClientIp])
									when 1	
										sent_mail.delay.save_open(params[:ClientIp])
									when 2
										sent_mail.delay.save_unsubscribe(params[:ClientIp])
								end				
							when "Complain"
								sent_mail.spam = Time.now
							when "Failed"
								sent_mail.bounced = Time.now
							when "Validation"
						end
						sent_mail.save		
					end			
	
				when "elasticemail"
					sent_mail = SentMail.find_by(ext_id: params[:transaction])
					if sent_mail
						case params[:status]
							when "Opened"
								sent_mail.opened = Time.now
							when "Clicked"
								sent_mail.save_click(params[:target])
							when "Error"
								sent_mail.bounced = Time.now
							when "Sent"
								sent_mail.delivered = Time.now
							when "AbuseReport"
								sent_mail.spam = Time.now
							when "Unsubscribed"
								sent_mail.create_unsubscribe
						end
						sent_mail.save
					end		
			end
		end
	
		#render plain: "#{params}"
		render status: 200, json: @controller.to_json
	end

	def hello
		render plain: "Hello, world!"
	end

	# See: http://help.mandrill.com/entries/23704122-Authenticating-webhook-requests
  def verify_mandrill_request_signature(signed_data, post_params)
    #signed_data = request.url
    #post_params = request.request_parameters.dup # POST parameters

    unless signed_data.blank? || post_params.blank? || ENV['MANDRILL_WEBHOOK_KEY'].blank?
    	signed_data += request.request_parameters.sort.join

    	signature = Base64.strict_encode64(OpenSSL::HMAC.digest('sha1',ENV['MANDRILL_WEBHOOK_KEY'],signed_data)) || nil
	else
		signature = nil
	end
    # logger.debug("our: #{signature}, mandrill: #{request.headers['X-Mandrill-Signature']}")
		# puts "our: #{signature},\nmandrill: #{request.headers['X-Mandrill-Signature']}"
		if signature == request.headers['X-Mandrill-Signature'] && signature != nil
			return true
		else
			return false
		end
    # Do something here.. compare them..
  end

  private

  def mailgun_verify(api_key, token, timestamp, signature)
    digest = OpenSSL::Digest::SHA256.new
    data = [timestamp, token].join
    signature == OpenSSL::HMAC.hexdigest(digest, api_key, data)
  end

  def triggers_test(event_type, sent_mail_id)
  	case "#{event_type}"
  		when "processed"
  			# Placeholder.
		when "dropped"
			# Placeholder.
		when "delivered"
			# Placeholder.
		when "deferred"
			# Placeholder.
		when "bounce"
			# Placeholder.
		when "open"
			trigger_arr = [3, 4]

		when "click"
			trigger_arr = [2, 8, 9]

		when "spamreport"
			# Placeholder.
		when "unsubscribe"
			trigger_arr = [6]

		when "group_unsubscribe"
			# Placeholder.
		when "group_resubscribe"
			# Placeholder.
  	end

  	unless trigger_arr.blank?
  		# Get the list/bc/etc. id.
		sent_mail = SentMail.select(:message_type, :message_id).find(sent_mail_id)
		if sent_mail.message_type = "Broadcast"
			broadcast = Broadcast.select(:id, :list_id).find(sent_mail.message_id)
			followup = nil
			list_id = broadcast.list_id
		else
			broadcast = nil
			followup = nil
			list_id = followup.list_id
		end

		open_events = Followup.where(["active IS TRUE user_id = ? AND list_id = ? AND trigger_id IN (?)", current_user.id, list_id, trigger_arr])

		unless open_events.blank?
			triggers = FollowupTrigger.where(["id IN [?]", open_events.map { |o| o.trigger_id }])

			# Run the trigger code to push eligible messages into the fu queue.
			triggers.each do |trigger|
				eval(trigger.code)
			end
		end
  	end
  end
end