class ServicesUser < ActiveRecord::Base
	after_save :activate_webhooks
	belongs_to :service

	def activate_webhooks
		usage = JSON.parse(self.usage)

		case self.service_id
			when 1
				
			when 2
				# Sendgrid
				activate_string = "api_user=#{usage["api_name"]}&api_key=#{usage["api_key"]}&name=eventnotify&processed=0&dropped=1&deferred=0&delivered=1&bounce=1&click=1&unsubscribe=1&spamreport=1&url=http://45.55.217.219/webhooks/sendgrid"

				#puts "Activate Sendgrid Webhooks: #{activate_string}"
				 result = HTTParty.post("https://api.sendgrid.com/api/filter.setup.json",
				     	:body => activate_string,
				     	:headers => { 'Content-Type' => 'application/json' } )

			when 3
				# Mandrill
				activate_hash = { key: "#{usage["api_key"]}", url: "http://45.55.217.219/webhooks/mandrill", description: "Klickrr - Mandrill Webhook", events: ["send","hard_bounce","soft_bounce","open","click","spam","unsub","reject"] }
				activate_string = activate_hash.to_json

				 result = HTTParty.post("https://mandrillapp.com/api/1.0/webhooks/add.json",
				     	:body => activate_string,
				     	:headers => { 'Content-Type' => 'application/json; charset=utf-8' } )

			when 4
				# MailGun

				# For MailGun, we will have to create a different webhook for every event we intend to track.
				for x in 1...7
				  case x
				  	when 1
				  		id = "bounce"
				  		url = "http://45.55.217.219/webhooks/mailgun&event_name=bounce"
				  	when 2
				  		id = "deliver"
				  		url = "http://45.55.217.219/webhooks/mailgun&event_name=deliver"
				  	when 3
						id = "drop"
						url = "http://45.55.217.219/webhooks/mailgun&event_name=drop"
				  	when 4
						id = "spam"
						url = "http://45.55.217.219/webhooks/mailgun&event_name=spam"
				  	when 5
						id = "unsubscribe"
						url = "http://45.55.217.219/webhooks/mailgun&event_name=unsubscribe"
				  	when 6
						id = "click"
						url = "http://45.55.217.219/webhooks/mailgun&event_name=click"
				  	when 7
						id = "open"
						url = "http://45.55.217.219/webhooks/mailgun&event_name=open"
				  end
				  
				  result = RestClient.post("https://api:#{usage["api_key"]}"\
                  "@api.mailgun.net/v2/domains/45.55.217.219/webhooks/mailgun",
                  :id => "#{id}",
                  :url => "#{url}")

				  # result = HTTParty.post("https://api.mailgun.net/v2/domains/klickrr.net/webhooks",
      #             :query => post_params,
      #             :basic_auth => auth
      #             #:headers => {"Authorization" => "Api api:key-a69207983aae1647c1344903f03bbca7"}
      #             )

                  #puts "Mailgun webhook generation: #{result}"
				end

			when 5
				# MailJet
				usr = User.find(self.user_id)

				unless usage["from_email"].blank?
					email_hash = { Name: "#{usr.first_name} #{usr.last_name}", Email: "#{usage["from_email"]}" }
				else
					email_hash = { Name: "#{usr.first_name} #{usr.last_name}", Email: "#{usr.email}" }
				end
				email = email_hash.to_json

				begin
					result = HTTParty.post("https://#{usage["api_key"]}:#{usage["api_name"]}@api.mailjet.com/v3/REST/sender",
    				    	:body => email,
    				    	:headers => { "Content-Type" => "application/json" } )
					
					begin
						for event in 1..7 do
							case event
								when 1
									evt = "open"

								when 2
									evt = "click"

								when 3
									evt = "bounce"

								when 4
									evt = "spam"

								when 5
									evt = "blocked"

								when 6
									evt = "unsub"

								when 7
									evt = "sent"
							end

							mailjet_api_rec = HTTParty.get("https://#{usage["api_key"]}:#{usage["api_name"]}@api.mailjet.com/v3/REST/apikey")
							
							webhook = HTTParty.post("https://#{usage["api_key"]}:#{usage["api_name"]}@api.mailjet.com/v3/REST/eventcallbackurl",
								:body => { APIKeyID: "#{mailjet_api_rec["Data"][0]["ID"]}", EventType: "#{evt}",  Url: 'http://45.55.217.219/webhooks/mailjet'}.to_json,
								:headers => {"Content-Type" => "application/json"} )

							# puts "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
							# puts "Conn string: https://#{usage["api_key"]}:#{usage["api_name"]}@api.mailjet.com/v3/REST/eventcallbackurl"
							# puts "~~ Response: #{webhook.inspect}"
							# puts "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
						end

					rescue Exception => err
						puts "Error: #{err.inspect}"
					end
				rescue Exception => e
					puts "Error: #{e.inspect}"
				end

			when 6
				# Amazon SES
				# Using internal "lite" tracking - no webhooks to set up.

			when 7
				# Postage
				# Using internal "lite" tracking - no webhooks to set up.

			when 8
				# Socket Labs

			when 9
				# Elastic Email

		end
	end
end