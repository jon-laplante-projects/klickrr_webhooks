class OpenedMail < ActiveRecord::Base
  belongs_to :message, polymorphic: true
end