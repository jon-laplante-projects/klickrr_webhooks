# == Schema Information
#
# Table name: segments
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  segment_id :integer
#  action_id  :integer
#  variables  :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  seg_type   :string
#

class Segment < ActiveRecord::Base
	# attr_accessor :broadcast_id
	# attr_accessor :followup_id

	# belongs_to :broadcast, -> {where seg_type: 'broadcast'}
	# belongs_to :followup, -> {where seg_type: 'followup'}

	# def broadcast_id
	# 	if self.seg_type === "broadcast"
	# 		broadcast_id = self.action_id
	# 	else
	# 		broadcast_id = nil
	# 	end
	# end

	# def followup_id
	# 	if self.seg_type === "followup"
	# 		followup_id = self.action_id
	# 	else
	# 		followup_id = nil
	# 	end
	# end
end
