class ServicesUsers < ActiveRecord::Base
	after_save :activate_webhooks

	def activate_webhooks
		usage = JSON.parse(self.usage)

		case self.service_id
			when 1
				
			when 2
				# Sendgrid
				activate_string = "api_user=#{usage["api_name"]}&api_key=#{usage["api_key"]}&name=eventnotify&processed=0&dropped=1&deferred=0&delivered=1&bounce=1&click=1&unsubscribe=1&spamreport=1&url=http://klickrr.net/webhooks/incoming?service=sendgrid"

				#puts "Activate Sendgrid Webhooks: #{activate_string}"
				 result = HTTParty.post("https://api.sendgrid.com/api/filter.setup.json",
				     	:body => activate_string,
				     	:headers => { 'Content-Type' => 'application/json' } )

			when 3
				# Mandrill
				activate_hash = { key: "#{usage["api_key"]}", url: "http://klickrr.net/webhooks/incoming?service=mandrill", description: "Klickrr - Mandrill Webhook", events: ["send","hard_bounce","soft_bounce","open","click","spam","unsub","reject"] }
				activate_string = activate_hash.to_json

				 result = HTTParty.post("https://mandrillapp.com/api/1.0/webhooks/add.json",
				     	:body => activate_string,
				     	:headers => { 'Content-Type' => 'application/json; charset=utf-8' } )

			when 4
				# MailGun

				# For MailGun, we will have to create a different webhook for every event we intend to track.
				for x in 1...7
				  case x
				  	when 1
				  		id = "bounce"
				  		url = "http://klickrr.net/webhooks/incoming?service=mailgun&event_name=bounce"
				  	when 2
				  		id = "deliver"
				  		url = "http://klickrr.net/webhooks/incoming?service=mailgun&event_name=deliver"
				  	when 3
						id = "drop"
						url = "http://klickrr.net/webhooks/incoming?service=mailgun&event_name=drop"
				  	when 4
						id = "spam"
						url = "http://klickrr.net/webhooks/incoming?service=mailgun&event_name=spam"
				  	when 5
						id = "unsubscribe"
						url = "http://klickrr.net/webhooks/incoming?service=mailgun&event_name=unsubscribe"
				  	when 6
						id = "click"
						url = "http://klickrr.net/webhooks/incoming?service=mailgun&event_name=click"
				  	when 7
						id = "open"
						url = "http://klickrr.net/webhooks/incoming?service=mailgun&event_name=open"
				  end
				  
				  result = RestClient.post("https://api:#{usage["api_key"]}"\
                  "@api.mailgun.net/v2/domains/klickrr.net/webhooks",
                  :id => "#{id}",
                  :url => "#{url}")

				  # result = HTTParty.post("https://api.mailgun.net/v2/domains/klickrr.net/webhooks",
      #             :query => post_params,
      #             :basic_auth => auth
      #             #:headers => {"Authorization" => "Api api:key-a69207983aae1647c1344903f03bbca7"}
      #             )

                  #puts "Mailgun webhook generation: #{result}"
				end

			when 5
				# MailJet
				query_hash = { APIKey: "#{usage["api_key"]}" }
				query = query_hash.to_json

				result = RestClient.post("https://#{usage["api_key"]}:#{usage["api_name"]}"\
					"@api.mailjet.com/v3/REST/eventcallbackurl",
					:body => query,
					:headers => { 'Content-Type' => 'application/json; charset=utf-8' })

				puts "MailJet results: #{result}"
				# curl -s -X POST \
			 #    --user "$MJ_APIKEY_PUBLIC:$MJ_APIKEY_PRIVATE" \
			 #    https://api.mailjet.com/v3/REST/eventcallbackurl \
			 #    -H 'Content-Type: application/json' \
			 #    -d '{"APIKey": ...}'

			when 6
				# Amazon SES

			when 7
				# Postage
				#####################################################
				## No webhooks for Postage - requires
				## hourly polling.
				#####################################################
			when 8
				# Socket Labs

			when 9
				# Elastic Email

		end
	end
end