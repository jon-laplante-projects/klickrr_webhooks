class Followup < ActiveRecord::Base
  belongs_to :type, foreign_key: :type_id, class_name: "FollowupType"
  belongs_to :trigger, foreign_key: :trigger_id, class_name: "FollowupTrigger"
  belongs_to :scope, foreign_key: :scope_id, class_name: "FollowupScope"
  # belongs_to :broadcast
  # has_many :followup_segments, dependent: :destroy
  # has_many :broadcast_followups, dependent: :destroy
  has_many :followup_message_parts, dependent: :destroy
  has_many :queued_messages, dependent: :destroy
  has_many :sent_mails, through: :user
  belongs_to :user
  has_many :opened_mail, as: :message
  has_many :sent_email_followups

  def trigger_description
    description = self.trigger.try(:descr)
    variables = self.trigger_variables
    eval(variables).each { |key, value| description.gsub!("{{#{key}}}", value) } if description.present? && variables.present?
    description
  end

end
