# == Schema Information
#
# Table name: sent_mails
#
#  id               :integer          not null, primary key
#  user_id          :integer
#  service_id       :integer
#  message_id       :integer
#  message_type     :string(255)
#  email            :string(255)
#  clicks           :text
#  created_at       :datetime
#  updated_at       :datetime
#  tracking_code    :string(255)
#  contact_id       :integer
#  sent             :boolean
#  response_message :text
#  opened           :datetime
#  dropped          :datetime
#  delivered        :datetime
#  bounced          :datetime
#  unsubscribed     :datetime
#  spam             :datetime
#  blocked          :datetime
#  ext_id           :string
#  status           :string
#  dirty            :boolean
#

class SentMail < ActiveRecord::Base
  include ApplicationHelper
  belongs_to :list
  belongs_to :user
  has_many :followups, through: :user

  GEOIP = GeoIP.new(Rails.root.join('lib/GeoLiteCity.dat'))

  def generate_token(column, length = 64)
    begin
      self[column] = SecureRandom.urlsafe_base64 length
    end while SentMail.exists?(column => self[column])
  end

  def save_click(url, ip = nil)
    click = { url: url, clicked_at: Time.now.to_s, ip_addr: ip }
    add_to_collection(:clicks, click)
  end

  def save_open(ip = nil)
    open_event = { opened_at: Time.now.to_s, ip_addr: ip }
    add_to_collection(:opened, open_event)
  end

  def save_unsubscribe(ip = nil)
    unsubscribe_event = { unsubscribed_at: Time.now.to_s, ip_addr: ip }
    add_to_collection(:unsubscribed, unsubscribe_event)
  end

  private

  def search_location(ip)
    [:opened, :clicks, :unsubscribed].each do |collection|
      if SentMail.last[collection].present?
        eval(SentMail.last[collection]).each do |item|
          location = item.slice(:city, :region, :country, :timezone)
          return location if item[:ip_addr] == ip && location.present? && location.values.all? { |x| !x.nil? }
        end  
      end
    end
    nil  
  end  

  def get_location(ip)
    if ip.present?
      result = GEOIP.city(ip)
      if result.present?
        {
          city: result.city_name,
          region: result.real_region_name,
          country: result.country_name,
          timezone: result.timezone
        }
      end
    end    
  end

  def add_to_collection(collection, item)
    item.merge!({city: nil, region: nil, country: nil, timezone: nil})
    if item[:ip_addr].present?
      location = search_location(item[:ip_addr])
      location ||= get_location(item[:ip_addr])
      item.merge!(location) if location.present?
    end  

    arr = [] 
    arr = eval(self[collection]) if self[collection].present?
    arr << item
    update_attribute(collection, arr.to_s)
  end 

end
