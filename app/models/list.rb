# == Schema Information
#
# Table name: lists
#
#  id           :integer          not null, primary key
#  title        :string(255)
#  created_at   :datetime
#  updated_at   :datetime
#  user_id      :integer
#  link_string  :string(255)
#  sender       :string(255)
#  sender_email :string(255)
#  domain       :string(255)
#  services     :string
#

class List < ActiveRecord::Base
	has_many :list_field_groups, dependent: :destroy
	has_many :list_fields, dependent: :destroy
	has_many :broadcasts, dependent: :destroy
	has_many :followups, dependent: :destroy
	has_many :sent_mail

	belongs_to :user
end
