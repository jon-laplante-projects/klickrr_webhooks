# == Schema Information
#
# Table name: broadcast_segments
#
#  id           :integer
#  user_id      :integer
#  segment_id   :integer
#  broadcast_id :integer
#  created_at   :datetime
#  updated_at   :datetime
#

class BroadcastSegment < ActiveRecord::Base
	belongs_to :broadcast
end
