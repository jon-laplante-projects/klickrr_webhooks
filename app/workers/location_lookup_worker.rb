class LocationLookupWorker
  include Shoryuken::Worker

  shoryuken_options queue: 'location_lookup', auto_delete: true, body_parser: JSON
  GEOIP = GeoIP.new(Rails.root.join('lib/GeoLiteCity.dat'))
  def perform(sqs_msg, body)
    result = GEOIP.city(body["request_ip"])
    if result.present?
      sent_mail = SentMail.find(body["email_id"])
      sent_mail.update_attributes(country: result.country_name,
                                  city: result.city_name)
    end
  end

end
