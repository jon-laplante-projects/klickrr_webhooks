desc "This task downloads fresh databases for geoip"
task download_geoip_database: :environment do
  `cd lib;
  rm GeoIPASNum.dat.gz GeoLiteCity.dat.gz GeoIP.dat.gz;
  wget http://geolite.maxmind.com/download/geoip/database/GeoLiteCountry/GeoIP.dat.gz;
  wget http://geolite.maxmind.com/download/geoip/database/GeoLiteCity.dat.gz;
  wget http://geolite.maxmind.com/download/geoip/database/asnum/GeoIPASNum.dat.gz;
  gunzip -f *.gz`
end